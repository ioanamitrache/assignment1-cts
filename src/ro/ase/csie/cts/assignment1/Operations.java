package ro.ase.csie.cts.assignment1;

import java.util.Vector;

public interface Operations {
	public void InitializeReceiverHostsVector(Vector receiverHostIPs);
	public void AddReceiverHost(String receiverHostIP);
	public void PrintReceiverHostsIPs();
	public void PrintSendingHostIP();
}
