package ro.ase.csie.cts.assignment1;

import java.util.Vector;

public class TestGit {

	public static void main(String[] args) {
		System.out.println("Hello Git! The name of the license project is [Networking program installer]");
			
		Multicast firstMulticastTest = new Multicast();
		firstMulticastTest.PrintSendingHostIP();
		firstMulticastTest.PrintReceiverHostsIPs();
		
		Multicast secondMulticastTest = new Multicast("192.168.1.1", 2);
		Vector IPs = new Vector(2);
		IPs.add(0, "192.168.0.1");
		IPs.add(1, "192.168.1.254");
		secondMulticastTest.InitializeReceiverHostsVector(IPs);
		secondMulticastTest.AddReceiverHost("192.168.8.1");
		secondMulticastTest.PrintSendingHostIP();
		secondMulticastTest.PrintReceiverHostsIPs();
		
	}

}
