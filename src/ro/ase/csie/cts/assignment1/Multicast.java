package ro.ase.csie.cts.assignment1;

import java.util.Vector;

public class Multicast implements Operations{
	private String sendingHostIP;
	private int noReceiverHosts;
	private Vector receiverHostsIPs;
	
	public Multicast()
	{
		this.sendingHostIP = "";
		this.noReceiverHosts = 0;
		this.receiverHostsIPs = null;
	}
	
	public Multicast(String sendingHostIP, int noReceiverHosts)
	{
		this.sendingHostIP = sendingHostIP;
		this.noReceiverHosts = noReceiverHosts;
		this.receiverHostsIPs = new Vector(noReceiverHosts);
	}

	@Override
	public void AddReceiverHost(String receiverHostIP) {
		if(this.noReceiverHosts != 0)
		{
			receiverHostsIPs.add(receiverHostIP);
			this.noReceiverHosts ++;
		}
		else
		{
			receiverHostsIPs = new Vector();
			receiverHostsIPs.add(receiverHostIP);
			this.noReceiverHosts ++;
		}
	}

	@Override
	public void PrintReceiverHostsIPs() {
		if(this.noReceiverHosts!=0)
		{
			System.out.println("No. of receiver hosts: " + this.noReceiverHosts);
			for(int i=0; i<this.noReceiverHosts; i++)
				System.out.println(i+1 +") " + receiverHostsIPs.elementAt(i));
		}
		else
			System.out.println("No receiver hosts");
	}

	@Override
	public void PrintSendingHostIP() {
		if(this.sendingHostIP.equals(""))
			System.out.println("Uninitialized sending host");
		else
			System.out.println("Administrator(sending host): " + this.sendingHostIP);
	}

	@Override
	public void InitializeReceiverHostsVector(Vector receiverHostIPs) {
		if(receiverHostIPs.capacity() != this.noReceiverHosts)
			return;
		else
			for(int i=0; i<this.noReceiverHosts; i++)
				this.receiverHostsIPs.add(i,receiverHostIPs.elementAt(i));
	}

}
